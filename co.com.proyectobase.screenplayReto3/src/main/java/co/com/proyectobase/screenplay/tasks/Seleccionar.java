package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.OrangeHRSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Seleccionar implements Task{
	
	

	private OrangeHRSitePage orangeHRSitePage;
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_PIM));
		actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_ADD_EMPLOYEE));
		actor.attemptsTo(Esperar.aMoment());
		
	}

	public static Performable SeleccionarMenu() {
		return Tasks.instrumented(Seleccionar.class);
		
	}
	
	
}
