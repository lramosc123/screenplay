#Author: your.email@your.domain.com
Feature: Dado que Juan necesita crear un empleado en el OrageHR
Cuando el realiza el ingreso del registro en la aplicación
Entonces el visualiza el nuevo empleado en el aplicativo


  @Login
  Scenario: Ingresar al login OrangeHRM
  Given Juan quiere ingresar a OrangeHRM
  When El quiere ingresar al login
  Then El verifica la página principal OrangeHRM 
 
  
  @CrearEmpleado
  Scenario: Crear empleado
  Given Juan quiere crear un empleado  
  When El ingresa al menu crear empleado
  |firstname|nickname|lastname|location|
  |Leidy|LR|Ramos |Indian|
  Then El guarda los datos del empleado
  
  @BuscarEmpleado
  Scenario: Buscar empleado
  Given Juan quiere buscar un empleado 
  |filter|
  |0118|
  Then El confirma los datos del empleado
  