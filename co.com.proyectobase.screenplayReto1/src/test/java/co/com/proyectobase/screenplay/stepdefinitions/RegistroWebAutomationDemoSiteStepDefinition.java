package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.DatosPersonales;
import co.com.proyectobase.screenplay.tasks.Consultar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class RegistroWebAutomationDemoSiteStepDefinition {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");

	
	
	@Before
	public void configuracionInicial() {
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^Carlos ingresa a la Página Automation Demo Site$")
	public void carlos_ingresa_a_la_Página_Automation_Demo_Site() throws Exception {
		carlos.wasAbleTo(Ingresar.WebAutomation());
	}

	@When("^El quiere diligenciar todos los campos para el registro en la pantalla con los siguientes datos$")
	public void el_quiere_diligenciar_todos_los_campos_para_el_registro_en_la_pantalla_con_los_siguientes_datos(List<DatosPersonales>datospersonales) throws Exception {
		carlos.attemptsTo(Consultar.InformacionPersonal(datospersonales));
	}

	@Then("^El verifica que se registró correctamente$")
	public void el_verifica_que_se_registró_correctamente() throws Exception {
	   
	    
	}
	
	
	
	
	
}
