package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.choucairtesting.com/MaxTimeCHC/Login.aspx?ReturnUrl=%2fMaxTimeCHC%2f#ShortcutViewID=ReporteDiaSoloAnalista_ListView&ShortcutObjectClassName=MaxTime.Module.BusinessObjects.ReporteDia")
public class MaxtimePage extends PageObject{
    
	
	public static final Target USER_TEXT_FIELD = Target.the("YOU CAN WRITE THE USER").located(By.xpath("//*[@id=\'Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I\']"));
	public static final Target PASSWORD_TEXT_FIELD = Target.the("YOU CAN WRITE THE PASSWORD").located(By.xpath("//*[@id=\'Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I\']"));
	public static final Target CONECTARSE_BUTTON = Target.the("BUTTON FOR LOGIN").located(By.xpath("//*[@id=\'Logon_PopupActions_Menu_DXI0_T\']/a"));
	public static final Target DATE_LINK = Target.the("LINK FOR DATE").located(By.xpath("//*[@id=\'Vertical_v1_LE_v2_cell0_0_xaf_Fecha\']/tbody/tr/td"));
	
	
}
