package co.com.proyectobase.screenplay.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import co.com.proyectobase.screenplay.ui.OrangeHRSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public  class Abrir implements Task {

private OrangeHRSitePage orangeHRSitePage;	
	
@Override
public <T extends Actor> void performAs(T actor) {
	actor.attemptsTo(Open.browserOn(orangeHRSitePage));
		
}

public static Abrir OrangeHR() {
	return instrumented(Abrir.class);
}

	
}
