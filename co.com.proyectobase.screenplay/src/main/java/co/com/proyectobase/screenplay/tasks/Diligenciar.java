package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.Usuarios;
import co.com.proyectobase.screenplay.ui.ElTiempoFormulario;
import co.com.proyectobase.screenplay.ui.ElTiempoPage;
import co.com.proyectobase.screenplay.ui.GoogleHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Diligenciar implements Task {

	private List<Usuarios>usuarios;
	
	

	
	public Diligenciar(List<Usuarios> usuarios) {
		this.usuarios = usuarios;
	}

	private ElTiempoPage eltiempopage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		eltiempopage.getDriver().switchTo().frame("iframe_registro");
		actor.attemptsTo(Enter.theValue(usuarios.get(0).getNombre()).into(ElTiempoFormulario.CAMPO_NOMBRE));
		actor.attemptsTo(Enter.theValue(usuarios.get(0).getApellidos()).into(ElTiempoFormulario.CAMPO_APELLIDO));
		actor.attemptsTo(Enter.theValue(usuarios.get(0).getCorreo()).into(ElTiempoFormulario.CAMPO_CORREO));
		actor.attemptsTo(Enter.theValue(usuarios.get(0).getClave()).into(ElTiempoFormulario.CAMPO_CLAVE));
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_APLICACIONES));
		
	}

	public static Performable FormularioIngreso(List<Usuarios>usuarios) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Diligenciar.class, usuarios);
	}
	
	

	
	
	
}
