package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.model.DatosPersonales;
import co.com.proyectobase.screenplay.ui.WebAutomationDemoSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Consultar implements Task{

	private List<DatosPersonales>datospersonales;
	
	public Consultar(List<DatosPersonales> datospersonales) {
		this.datospersonales = datospersonales;
	}
	
	private WebAutomationDemoSitePage webautomationdemosite;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getFirstname()).into(WebAutomationDemoSitePage.CAMPO_FIRSTNAME));
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getLastname()).into(WebAutomationDemoSitePage.CAMPO_LASTNAME));
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getAddress()).into(WebAutomationDemoSitePage.CAMPO_ADDRESS));		
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getEmail()).into(WebAutomationDemoSitePage.CAMPO_EMAIL));
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getPhone()).into(WebAutomationDemoSitePage.CAMPO_PHONE));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.RADIOBUTTON_FEMALE));
		//actor.attemptsTo(Enter.theValue(datospersonales.get(0).getGendermale()).into(WebAutomationDemoSitePage.RADIOBUTTON_MALE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(datospersonales.get(0).getCountry()).from(WebAutomationDemoSitePage.CAMPO_COUNTRY));
		actor.attemptsTo(SelectFromOptions.byVisibleText(datospersonales.get(0).getYear()).from(WebAutomationDemoSitePage.CAMPO_YEAR));
		actor.attemptsTo(SelectFromOptions.byVisibleText(datospersonales.get(0).getMonth()).from(WebAutomationDemoSitePage.CAMPO_MONTH));
		actor.attemptsTo(SelectFromOptions.byVisibleText(datospersonales.get(0).getDay()).from(WebAutomationDemoSitePage.CAMPO_DAY));
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getPassword()).into(WebAutomationDemoSitePage.CAMPO_PASSWORD));
		actor.attemptsTo(Enter.theValue(datospersonales.get(0).getConfirmpassword()).into(WebAutomationDemoSitePage.CAMPO_CONFIRM_PASSWORD));
	    actor.attemptsTo(Click.on(WebAutomationDemoSitePage.BOTON_SUBMIT));
	    actor.attemptsTo(Esperar.aMoment());
	}

	public static Performable InformacionPersonal(List<DatosPersonales>datospersonales) {
		return Tasks.instrumented(Consultar.class, datospersonales);
		
	}
	
}
