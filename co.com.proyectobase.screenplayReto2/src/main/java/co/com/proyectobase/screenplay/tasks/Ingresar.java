package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.model.DatosPersonales;
import co.com.proyectobase.screenplay.ui.OrangeHRSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Ingresar implements Task{
	
private List<DatosPersonales>datospersonales;
	
	public Ingresar(List<DatosPersonales> datospersonales) {
		this.datospersonales = datospersonales;
	}

	private OrangeHRSitePage orangeHRSitePage;
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {			
	    actor.attemptsTo(Enter.theValue(datospersonales.get(0).getFirstname()).into(OrangeHRSitePage.CAMPO_FIRSTNAME));
	    actor.attemptsTo(Enter.theValue(datospersonales.get(0).getNickname()).into(OrangeHRSitePage.CAMPO_MIDDLENAME));
	    actor.attemptsTo(Enter.theValue(datospersonales.get(0).getLastname()).into(OrangeHRSitePage.CAMPO_LASTNAME));
	    actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_LOCATION));
	    actor.attemptsTo(Enter.theValue(datospersonales.get(0).getLocation()).into(OrangeHRSitePage.BOTON_LOCATION));
	    actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_LOCATION));
	    actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_SAVE));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_SAVE_ALL));
	}
	
	public static Performable InformacionPersonal(List<DatosPersonales>datospersonales) {
		return Tasks.instrumented(Ingresar.class, datospersonales);
		
	}
	

}
