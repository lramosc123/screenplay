package co.com.proyectobase.screenplay.interactions;

import static net.serenitybdd.screenplay.Tasks.instrumented;



import java.util.concurrent.TimeUnit;

 

import net.serenitybdd.screenplay.Actor;

import net.serenitybdd.screenplay.Interaction;

import org.awaitility.Awaitility;

import static co.com.proyectobase.screenplay.util.UtilidadTiempo.condicionExitosa;

 

public class Esperar implements Interaction {
                @Override
                public <T extends Actor> void performAs(T actor) {
                               try {
                                               Awaitility.await().forever().pollInterval(10000, TimeUnit.MILLISECONDS).until(condicionExitosa());
                               } catch (Exception e) {
                                               e.getMessage();
                               }
                             }

 
                public static Esperar aMoment() {
                               return instrumented(Esperar.class);

                }
 
}

 
