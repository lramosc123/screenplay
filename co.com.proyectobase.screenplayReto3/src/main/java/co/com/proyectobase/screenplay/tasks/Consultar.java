package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.model.DatosPersonales;
import co.com.proyectobase.screenplay.ui.OrangeHRSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Consultar implements Task{

	
	private OrangeHRSitePage orangeHRSitePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
	    actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_LOGIN));
	    actor.attemptsTo(Esperar.aMoment());  	    
	    
	    
	    
	}

	public static Performable Login() {
		return Tasks.instrumented(Consultar.class);
		
	}
	
}
