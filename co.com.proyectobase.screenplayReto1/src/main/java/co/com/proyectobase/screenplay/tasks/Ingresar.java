package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.WebAutomationDemoSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar  implements Task{
	
	WebAutomationDemoSitePage webAutomationDemoSitePage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.wasAbleTo(Open.browserOn(webAutomationDemoSitePage));
		
	}

	public static Ingresar WebAutomation() {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Ingresar.class);
	}
	


	
	
	
}
