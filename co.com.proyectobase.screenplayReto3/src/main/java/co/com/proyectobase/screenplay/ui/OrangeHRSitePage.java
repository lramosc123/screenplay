package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class OrangeHRSitePage extends PageObject{

	public static final Target BOTON_LOGIN = Target.the("Botón para confirmar el login").located(By.id("btnLogin"));
	public static final Target BOTON_PIM = Target.the("Botón para el menú PIM").located(By.xpath("//*[@id=\'menu_pim_viewPimModule\']/a/span[2]"));
	public static final Target BOTON_ADD_EMPLOYEE = Target.the("Botón para el menú adicionar empleado").located(By.xpath("//*[@id=\'menu_pim_addEmployee\']/span[2]"));
	public static final Target CAMPO_FIRSTNAME = Target.the("Campo para ingresar el primer nombre").located(By.id("firstName"));
	public static final Target CAMPO_MIDDLENAME = Target.the("Campo para ingresar el nick").located(By.id("middleName"));
	public static final Target CAMPO_LASTNAME = Target.the("Campo para ingresar el apellido").located(By.id("lastName"));
	public static final Target BOTON_LOCATION = Target.the("Campo para seleccionar la localidad").located(By.xpath("//*[@id=\'location_inputfileddiv\']/div/input"));
	public static final Target BOTON_SAVE = Target.the("Botón que guarda los datos personales").located(By.id("systemUserSaveBtn"));
	public static final Target BOTON_SAVE_ALL = Target.the("Botón que guarda y confirma los datos personales").located(By.id("pimPersonalDetailsForm"));
	public static final Target BOTON_EMPLOYEELIST = Target.the("Botón que muestra la lista de empleados").located(By.id("menu_pim_viewEmployeeList"));
	public static final Target CAMPO_FILTER = Target.the("Campo para buscar un empleado").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BOTON_SEARCH = Target.the("Botón para buscar un empleado").located(By.id("quick_search_icon"));
	
	
}
