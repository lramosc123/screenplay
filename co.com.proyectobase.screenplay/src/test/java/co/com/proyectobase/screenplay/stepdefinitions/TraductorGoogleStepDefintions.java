package co.com.proyectobase.screenplay.stepdefinitions;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import static org.hamcrest.Matchers.equalTo;

public class TraductorGoogleStepDefintions {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor rafa = Actor.named("Rafa");
	
	@Before
	public void configuracionInicial() {
		
		rafa.can(BrowseTheWeb.with(hisBrowser));
	}
	
	
	@Given("^That Rafa wants to use Google Traslate$")
	public void that_Rafa_wants_to_use_Google_Traslate() throws Exception {
		rafa.wasAbleTo(Abrir.LaPaginaDeGoogle());
		
	}
	
	@When("^He traslates the word (.*) from English to Spanish$")
	public void hetraslatesthewordtablefromEnglishtoSpanish(String palabra) {
		rafa.attemptsTo(Traducir.DeInglesAEspanolLa(palabra));
		
		
		
	}
	
	 @Then("^He should watch (.*) on the screen$")
	 public void he_should_watch_Mesa_on_the_screen(String palabraesperada) {
		 rafa.should(seeThat(LaRespuesta.es(), equalTo(palabraesperada)));
		 
	 }
	
}
