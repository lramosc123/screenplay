#Author: ljramos@bancolombia.com.co


Feature: Dado que Carlos quiere acceder a la Web Automation Demo Site
Cuando el realiza el registro en la página
Entonces el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row


  @Registro
  Scenario: Carlos quiere acceder a la Web Automation Demo Site
    Given Carlos ingresa a la Página Automation Demo Site
    When El quiere diligenciar todos los campos para el registro en la pantalla con los siguientes datos
    |firstname|lastname|address|email|phone|genderfemale|country|year|month|day|password|confirmpassword|
    |Leidy|Ramos|cll 55 |leidyramos3759@gmail.com|3013446579|FeMale|Colombia|1992|June|18|Siclaro4788|Siclaro4788|
    Then El verifica que se registró correctamente 

