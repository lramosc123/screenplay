package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.DatosPersonales;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Buscar;
import co.com.proyectobase.screenplay.tasks.Consultar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.Seleccionar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class OrangeHRSiteStepDefinition {

	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");	
	
	
	@Before
	public void configuracionInicial() {
		juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	//Login
	
	@Given("^Juan quiere ingresar a OrangeHRM$")
	public void juanquiereingresaraOrangeHRM() throws Exception {
		juan.wasAbleTo(Abrir.OrangeHR());
	}
	
	@When("^El quiere ingresar al login$")
	public void elquiereingresarallogin() throws Exception {
		juan.wasAbleTo(Consultar.Login());
	}
	
	@Then("^El verifica la página principal OrangeHRM$")
	public void elverificalapáginaprincipalOrangeHRM () throws Exception {
		
	}
	
	//Crear Empleado
	
	@Given("^Juan quiere crear un empleado$")
	public void juanquierecrearunempleado() throws Exception {
		juan.wasAbleTo(Seleccionar.SeleccionarMenu());
	}
	
	@When("^El ingresa al menu crear empleado$")
	public void elingresaalmenucrearempleado(List<DatosPersonales>datospersonales) throws Exception {
		juan.wasAbleTo(Ingresar.InformacionPersonal(datospersonales));
	}
	
	@Then("^El guarda los datos del empleado$")
	public void elguardalosdatosdelempleado() throws Exception {
		
	}
	
	//Buscar empleado
	
	
	@Given ("^Juan quiere buscar un empleado$")
	public void juanquierebuscarunempleado(List<DatosPersonales>datospersonales) throws Exception {	
		juan.wasAbleTo(Buscar.InformacionPersonal(datospersonales));
	}
		
	
	@Then("^El confirma los datos del empleado$")
    public void elconfirmalosdatosdelempleado() throws Exception {
		
	}
	
	
	
}




