package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;
import co.com.proyectobase.screenplay.model.Login;
import co.com.proyectobase.screenplay.tasks.EnterToLogin;
import co.com.proyectobase.screenplay.tasks.OpenMaxtime;
import co.com.proyectobase.screenplay.tasks.SelectDate;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class MaxtimeStepDefinition {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor leidy = Actor.named("Leidy");	
	
	
	@Before
	public void configuracionInicial() {
		leidy.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^Leidy wants to enter to MaxtimePage$")
	public void leidywantstoentertoMaxtimePage() throws Exception {
		leidy.wasAbleTo(OpenMaxtime.Page());
	}
	
	@When("^She writes user and password$")
	public void shewritesuserandpassword(List<Login>login) throws Exception {
		leidy.wasAbleTo(EnterToLogin.Login(login));		
	}
	
	@Then("^She enters to MaxtimePage$")
	public void sheenterstoMaxtimePage() throws Exception {
		leidy.wasAbleTo(SelectDate.Day());		
	}
	
		
	
	
}
