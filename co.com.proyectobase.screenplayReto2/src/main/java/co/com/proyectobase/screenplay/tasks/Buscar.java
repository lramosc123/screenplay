package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.model.DatosPersonales;
import co.com.proyectobase.screenplay.ui.OrangeHRSitePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Buscar implements Task{

private List<DatosPersonales>datospersonales;
	
	public Buscar(List<DatosPersonales> datospersonales) {
		this.datospersonales = datospersonales;
	}

	private OrangeHRSitePage orangeHRSitePage;
		
	
	@Override
	public <T extends Actor> void performAs(T actor) {
	actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_PIM));
	actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_EMPLOYEELIST));
	actor.attemptsTo(Esperar.aMoment());
	actor.attemptsTo(Enter.theValue(datospersonales.get(0).getFilter()).into(OrangeHRSitePage.CAMPO_FILTER));
	actor.attemptsTo(Click.on(OrangeHRSitePage.BOTON_SEARCH));
	actor.attemptsTo(Esperar.aMoment());
		
	}
	
	public static Performable InformacionPersonal(List<DatosPersonales>datospersonales) {
		return Tasks.instrumented(Buscar.class, datospersonales);
		
	}

}
