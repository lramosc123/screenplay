package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;


public class ElTiempoFormulario {

	
	public static final Target CAMPO_NOMBRE = Target.the("el campo Nombre").located(By.id("first_name"));
	public static final Target CAMPO_APELLIDO = Target.the("el campo Apellidos").located(By.id("last_name"));
	public static final Target CAMPO_CORREO = Target.the("el campo Correo").located(By.id("email"));
	public static final Target CAMPO_CLAVE = Target.the("el campo Clave").located(By.id("password"));
	

}
