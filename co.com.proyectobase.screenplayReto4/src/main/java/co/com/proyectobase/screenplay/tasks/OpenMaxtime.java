package co.com.proyectobase.screenplay.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import co.com.proyectobase.screenplay.ui.MaxtimePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class OpenMaxtime implements Task{
	
private MaxtimePage maxtimepage;
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(maxtimepage));
		
	}
	
	public static OpenMaxtime Page() {
		return instrumented(OpenMaxtime.class);
	}
	
	
	
}
