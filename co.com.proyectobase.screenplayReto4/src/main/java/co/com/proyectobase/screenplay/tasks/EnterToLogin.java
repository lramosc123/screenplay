package co.com.proyectobase.screenplay.tasks;

import java.util.List;


import co.com.proyectobase.screenplay.model.Login;
import co.com.proyectobase.screenplay.ui.MaxtimePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class EnterToLogin implements Task{
	
private List<Login>login;
	
	public EnterToLogin(List<Login> login) {
		this.login = login;
	}
	
	
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(MaxtimePage.USER_TEXT_FIELD));
		actor.attemptsTo(Enter.theValue(login.get(0).getUser()).into(MaxtimePage.USER_TEXT_FIELD));
		actor.attemptsTo(Enter.theValue(login.get(0).getPassword()).into(MaxtimePage.PASSWORD_TEXT_FIELD));
		actor.attemptsTo(Click.on(MaxtimePage.CONECTARSE_BUTTON));
		
		
	}
	
	public static Performable Login(List<Login>login) {
		return Tasks.instrumented(EnterToLogin.class, login);
		
	}

}
