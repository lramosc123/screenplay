#Author: lramosc@choucairtesting.com
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario: Make a sign a doctor
    Given Carlos needs to sign a doctor   
    When He makes the sign in the Hospital Administration Application    
    Then He verifies in the screen the message that the dates are saved
   

  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with <name>
    When I check for the <value> in step
    Then I verify the <status> in step

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
