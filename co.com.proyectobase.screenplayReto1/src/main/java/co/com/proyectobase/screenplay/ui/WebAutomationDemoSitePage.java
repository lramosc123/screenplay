package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class WebAutomationDemoSitePage extends PageObject {

	public static final Target CAMPO_FIRSTNAME = Target.the("El campo que muestra el nombre").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[1]/input"));
	public static final Target CAMPO_LASTNAME = Target.the("El campo que muestra el apellido").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[2]/input"));
	public static final Target CAMPO_ADDRESS = Target.the("El campo que muestra la dirección").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[2]/div/textarea"));
	public static final Target CAMPO_EMAIL = Target.the("El campo que muestra el correo").located(By.xpath("//*[@id=\'eid\']/input"));
	public static final Target CAMPO_PHONE = Target.the("El campo que muestra el teléfono").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[4]/div/input"));
	public static final Target RADIOBUTTON_MALE = Target.the("La opción de género masculino").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[5]/div/label[1]/input"));
	public static final Target RADIOBUTTON_FEMALE = Target.the("La opción de género masculino").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[5]/div/label[2]/input"));                                                                                                           //*[@id="basicBootstrapForm"]/div[5]/div/label[2]/input
	public static final Target CHECK_CRICKET = Target.the("La opción de hobbies cricket").located(By.id("checkbox1"));
	public static final Target CHECK_MOVIES = Target.the("La opción de hobbies movies").located(By.id("checkbox2"));
	public static final Target CHECK_HOCKEY = Target.the("La opción de hobbies hockey").located(By.id("checkbox3"));
	public static final Target CAMPO_LANGUAGES = Target.the("El campo muestra idiomas").located(By.id("msdd"));
	public static final Target CAMPO_SKILLS = Target.the("El campo que muestra las tareas").located(By.id("Skills"));
	public static final Target CAMPO_COUNTRY = Target.the("El campo que muestra los paises").located(By.id("countries"));
	public static final Target CAMPO_COUNTRY2 = Target.the("El campo que muestra los paises 2").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[10]/div/span/span[1]/span"));
	public static final Target CAMPO_YEAR = Target.the("El campo que muestra los años").located(By.id("yearbox"));
	public static final Target CAMPO_MONTH = Target.the("El campo que muestra los meses").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[11]/div[2]/select"));
	public static final Target CAMPO_DAY = Target.the("El campo que muestra los dias").located(By.id("daybox"));
	public static final Target CAMPO_PASSWORD = Target.the("El campo que muestra la clave").located(By.id("firstpassword"));
	public static final Target CAMPO_CONFIRM_PASSWORD = Target.the("El campo que muestra la confirmación de la clave").located(By.id("secondpassword"));
	public static final Target BOTON_SUBMIT = Target.the("Botón para enviar la información").located(By.id("submitbtn"));
	public static final Target BOTON_REFRESH = Target.the("Botón para refrescar").located(By.id("Button1"));
	
	
}
