package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.MaxtimePage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class SelectDate implements Task{
	
	private MaxtimePage maxtimepage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(MaxtimePage.DATE_LINK));
	    actor.attemptsTo(Esperar.aMoment());
		
	}
	
	public static Performable Day() {
		return Tasks.instrumented(SelectDate.class);
		
	}

}
