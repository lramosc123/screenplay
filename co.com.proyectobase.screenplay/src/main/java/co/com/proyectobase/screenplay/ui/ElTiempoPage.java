package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.eltiempo.com/zona-usuario/crear")
public class ElTiempoPage  extends PageObject{
	
	
	public static final Target CHECK_ACEPTO = Target.the("El check que acepta los términos").located(By.xpath("//*[@id=\\'terms-div\\']/label"));
	public static final Target BOTON_CREAR_CUENTA = Target.the("El botón que muestra las aplicaciones").located(By.xpath("//*[@id=\\'submitbutton\\']"));
	
	
}
