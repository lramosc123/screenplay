package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.Usuarios;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class RegistroElTiempoStepDefinition {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor leidy = Actor.named("LEIDY");

	//leidy puede navegar en el navegador
	//Es capaz de 
	
	@Before
	public void configuracionInicial() {
		leidy.can(BrowseTheWeb.with(hisBrowser));
	}

	
	
	@Given("^Leidy quiere registrarse en la página del tiempo para leer las noticias$")
	public void leidy_quiere_registrarse_en_la_página_del_tiempo_para_leer_las_noticias() throws Exception {
	   leidy.wasAbleTo(Ingresar.LaPaginaTiempo());
	    
	}


	@When("^Ella ingresa a la página del tiempo y diligencia el formulario$")
	public void ella_ingresa_a_la_página_del_tiempo_y_diligencia_el_formulario(List<Usuarios>usuarios) throws Exception {
		leidy.attemptsTo(Diligenciar.FormularioIngreso(usuarios));
	   
	}

	@Then("^Ella puede ver las noticias con la cuenta LEIDY$")
	public void ella_puede_ver_las_noticias_con_la_cuenta_LEIDY() throws Exception {
	}
	
		
	
	
}
